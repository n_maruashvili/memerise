<?php
include 'database.php';
include 'login_signup.php';
?>
<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
<head>
    <link rel="stylesheet" href="style.css">
    <script src="js.js"></script>
</head>
<body>
<div class="bgimg w3-display-container w3-animate-opacity w3-text-white" >
    <!-- Sidebar -->
    <nav class="w3-sidebar w3-black w3-animate-top w3-xxlarge" style="display:none" id="mySidebar">
        <a href="javascript:void(0)" onclick="w3_close()" class="" >
            <button type="button" class="close" aria-label="Close" style="margin:15px; margin-top: 35px">
                <span aria-hidden="true" style="font-size: 40px; color: white">X</span>
            </button>
        </a>
        <div class="" style="position: relative; ">
            <ul class="nav navbar-nav" style="position: absolute">
                <li><a href="#" class="w3-bar-item w3-button w3-text-grey w3-hover-black" style="background-color: black" onclick="w3_open_login()">Log in</a></li>
                <li><a href="#" class="w3-bar-item w3-button w3-text-grey w3-hover-black" style="background-color: black" onclick="w3_open_signin()">Sign in</a></li>
                <li><a href="#" class="w3-bar-item w3-button w3-text-grey w3-hover-black" style="background-color: black" onclick="w3_open_about()">About</a></li>
                <li><a href="#" class="w3-bar-item w3-button w3-text-grey w3-hover-black" style="background-color: black" onclick="w3_open_contact()">Contact</a></li>
                <li><a href="#" class="w3-bar-item w3-button w3-text-grey w3-hover-black" style="background-color: black">Continue without log in</a></li>
            </ul>
        </div>
    </nav>
    <!-- edited -->
    <nav class="w3-sidebar w3-white w3-animate-top w3-xxlarge" style="display:none; background-color: white" id="mySidebar_login">
        <a href="javascript:void(0)" onclick="w3_close_login()" class="">
            <button type="button" class="close" aria-label="Close" style="margin:15px; margin-top: 35px">
                <span aria-hidden="true" style="font-size: 40px; color: black">X</span>
            </button>
        </a>
        <div class="container" style="position: relative; ">
            <form class="" style="position: absolute;  color: black; font-size: 25px;" method="post">

                <input type="text" name="username" class="w3-bar-item w3-button w3-text-grey w3-hover-black" placeholder="Enter Username" >
                <input type="password" name="password" class="w3-bar-item w3-button w3-text-grey w3-hover-black" placeholder="Enter Password">
                <input type="submit" class="w3-bar-item w3-button w3-text-grey w3-hover-black" name="login" value="Log in" style="font-size: 25px;">
            </form>
        </div>
    </nav>

    <nav class="w3-sidebar w3-white w3-animate-top w3-xxlarge" style="display:none; background-color: white" id="mySidebar_signin">
        <a href="javascript:void(0)" onclick="w3_close_signin()" class="">
            <button type="button" class="close" aria-label="Close" style="margin:15px; margin-top: 35px">
                <span aria-hidden="true" style="font-size: 40px; color: black">X</span>
            </button>
        </a>
        <div class="container" style="position: relative; ">
            <form class="" style="position: absolute;  color: black; font-size: 25px" method="post">

                <input type="text" name="username" class="w3-bar-item w3-button w3-text-grey w3-hover-black" placeholder="Enter Username" style="margin-top: 7px" >
                <input type="email" name="email" class="w3-bar-item w3-button w3-text-grey w3-hover-black" placeholder="Enter Email" style="margin-top: 7px">
                <input type="submit" class="w3-bar-item w3-button w3-text-grey w3-hover-black" name="login/register" value="Log in" style="font-size: 25px; margin-top: 7px; "><br>
                <input type="password" name="password1" class="w3-bar-item w3-button w3-text-grey w3-hover-black" placeholder="Enter Password" style="margin-top: 5px">
                <input type="password" name="password2" class="w3-bar-item w3-button w3-text-grey w3-hover-black" placeholder="Repeat Password" style="margin-top: 5px">

            </form>
        </div>
    </nav>

    <nav class="w3-sidebar w3-white w3-animate-top w3-xxlarge" style="display:none; background-color: white" id="mySidebar_about">
        <a href="javascript:void(0)" onclick="w3_close_about()" class="">
            <button type="button" class="close" aria-label="Close" style="margin:15px; margin-top: 35px">
                <span aria-hidden="true" style="font-size: 40px; color: black">X</span>
            </button>
        </a>
        <div class="container" style="position: relative; ">
            <form class="" style="position: absolute;  color: black; font-size: 25px; ">
                About: .......................................
            </form>
        </div>
    </nav>

    <nav class="w3-sidebar w3-white w3-animate-top w3-xxlarge" style="display:none; background-color: white" id="mySidebar_contact">
        <a href="javascript:void(0)" onclick="w3_close_contact()" class="">
            <button type="button" class="close" aria-label="Close" style="margin:15px; margin-top: 35px">
                <span aria-hidden="true" style="font-size: 40px; color: black">X</span>
            </button>
        </a>
        <div class="container" style="position: relative; ">
            <form class="" style="position: absolute;  color: black; font-size: 25px; ">
                Contact: .......................................
            </form>
        </div>
    </nav>
    <!-- edited -->

    <!-- Header -->
    <div class="w3-jumbo w3-animate-top">
        <div style="position:relative; width: 100%; background-color: rgba(213,231,236,0.6); height: 150px; text-align: center; padding-top: 20px" onclick="w3_open()">
            <div class="login" style="opacity: 1;"> <div> Join Us! </div> </div>

        </div>
    </div>
    <div class="w3-display-middle">
        <div class="w3-jumbo w3-animate-bottom">
            <div class="transbox" style="border-radius: 10px">
                <p>Make Memes Not War!</p>
            </div>
        </div>
        <hr class="w3-border-grey" style="margin:auto;width:40%">
        <div class="transbox" style="border-radius: 10px">
            <p style="color: black; font-weight: bolder; font-size: 40px; text-align: center">Brainstorm Of Nodar Maruashvili</p>
            <div>
            </div>
        </div>
    </div>
</div>
</body>
</html>