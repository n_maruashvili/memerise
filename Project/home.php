<?php
session_start();
if (!isset($_SESSION['username'])){
    header('location: main.php');
} else{
    echo 'you are logged in';
}
if (isset($_POST['logout'])){
    session_destroy();
    header('location: main.php');
}
?>
<html>
<form method="post">
    <input type="submit" name="logout" value="Log out">
</form>
</html>
