
// Open and close sidebar
function w3_open() {
    document.getElementById("mySidebar").style.width = "100%";
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("mySidebar").style.height = "123px";
}

function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
}

function w3_open_login() {
    document.getElementById("mySidebar_login").style.width = "100%";
    document.getElementById("mySidebar_login").style.display = "block";
    document.getElementById("mySidebar_login").style.height = "123px";
}

function w3_close_login() {
    document.getElementById("mySidebar_login").style.display = "none";
}

function w3_open_signin() {
    document.getElementById("mySidebar_signin").style.width = "100%";
    document.getElementById("mySidebar_signin").style.display = "block";
    document.getElementById("mySidebar_signin").style.height = "123px";
}

function w3_close_signin() {
    document.getElementById("mySidebar_signin").style.display = "none";
}

function w3_open_about() {
    document.getElementById("mySidebar_about").style.width = "100%";
    document.getElementById("mySidebar_about").style.display = "block";
    document.getElementById("mySidebar_about").style.height = "123px";
}

function w3_close_about() {
    document.getElementById("mySidebar_about").style.display = "none";
}

function w3_open_contact() {
    document.getElementById("mySidebar_contact").style.width = "100%";
    document.getElementById("mySidebar_contact").style.display = "block";
    document.getElementById("mySidebar_contact").style.height = "123px";
}

function w3_close_contact() {
    document.getElementById("mySidebar_contact").style.display = "none";
}